"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c, _d, _e, _f;
var HtmlIds;
(function (HtmlIds) {
    HtmlIds["addModifier"] = "addModifier";
    HtmlIds["difficultyPreset"] = "difficultyPreset";
    HtmlIds["difficultyThreshold"] = "difficultyThreshold";
    HtmlIds["reducedMotion"] = "reducedMotion";
    HtmlIds["diceAmount"] = "diceAmount";
    HtmlIds["faceAmount"] = "faceAmount";
    HtmlIds["criticalMargin"] = "criticalMargin";
    HtmlIds["rollButton"] = "rollButton";
    HtmlIds["resultText"] = "resultText";
    HtmlIds["resultDice"] = "resultDice";
    HtmlIds["flashoverSuccess"] = "flashover-success";
    HtmlIds["flashoverFailure"] = "flashover-failure";
    HtmlIds["sidebar"] = "sidebar";
})(HtmlIds || (HtmlIds = {}));
var CssAnimation;
(function (CssAnimation) {
    CssAnimation["none"] = "noanim";
    CssAnimation["fadeout"] = "fadeout";
    CssAnimation["furlin"] = "furlin";
    CssAnimation["furlout"] = "furlout";
    CssAnimation["rollin"] = "rollin";
})(CssAnimation || (CssAnimation = {}));
const AudioFiles = {
    spinup: new Audio('./spinup.mp3'),
    success: new Audio('./success.mp3'),
    failure: new Audio('./failure.mp3')
};
function getElementById(id) {
    return document.querySelector(`#${id}`);
}
function getInputValueById(id) {
    var _a;
    return (_a = getElementById(id)) === null || _a === void 0 ? void 0 : _a.value;
}
function getSelectedValueById(id) {
    var _a;
    return (_a = getElementById(id)) === null || _a === void 0 ? void 0 : _a.value;
}
function isReducedMotion() {
    var _a;
    return (_a = getElementById(HtmlIds.reducedMotion)) === null || _a === void 0 ? void 0 : _a.checked;
}
function animate(node, animationName) {
    return new Promise((resolve, reject) => {
        for (const a in CssAnimation) {
            const className = CssAnimation[a];
            node.classList.remove(className);
        }
        if (animationName == CssAnimation.none) {
            return;
        }
        node.classList.add(animationName);
        node.addEventListener('animationend', () => {
            resolve('animationend');
        }, { once: true });
    });
}
function clearChildren(element) {
    while (element.firstChild) {
        element.removeChild(element.lastChild);
    }
}
function delay(ms) {
    return new Promise(res => setTimeout(res, ms));
}
function doAfterDelay(ms, func, ...args) {
    return __awaiter(this, void 0, void 0, function* () {
        yield delay(ms);
        return func(args);
    });
}
//TODO move this hunk into a separate file
let diceSvgs = [];
{
    const svgns = 'http://www.w3.org/2000/svg';
    const svgcolor = '#fff';
    function generateSvgEye(cx, cy) {
        const eye = document.createElementNS(svgns, 'circle');
        eye.setAttribute('fill', svgcolor);
        eye.setAttribute('r', '18');
        eye.setAttribute('cx', cx.toString());
        eye.setAttribute('cy', cy.toString());
        return eye;
    }
    const topLeft = generateSvgEye(52, 52);
    const midLeft = generateSvgEye(52, 105);
    const bottomLeft = generateSvgEye(52, 158);
    const topMid = generateSvgEye(105, 52);
    const center = generateSvgEye(105, 105);
    const bottomMid = generateSvgEye(105, 158);
    const topRight = generateSvgEye(158, 52);
    const midRight = generateSvgEye(158, 105);
    const bottomRight = generateSvgEye(158, 158);
    function addDieFace(i, eyes) {
        const svg = document.createElementNS(svgns, 'svg');
        svg.setAttribute('width', '210');
        svg.setAttribute('height', '210');
        svg.setAttribute('xmlns', svgns);
        svg.setAttribute('class', 'die');
        const title = document.createElementNS(svgns, 'title');
        title.textContent = i.toString();
        svg.appendChild(title);
        const rect = document.createElementNS(svgns, 'rect');
        rect.setAttribute('width', '200');
        rect.setAttribute('height', '200');
        rect.setAttribute('fill', 'none');
        rect.setAttribute('stroke', svgcolor);
        rect.setAttribute('stroke-width', '10');
        rect.setAttribute('x', '5');
        rect.setAttribute('y', '5');
        rect.setAttribute('rx', '30');
        svg.appendChild(rect);
        eyes.forEach(e => {
            svg.appendChild(e.cloneNode());
        });
        diceSvgs[i] = svg;
    }
    addDieFace(0, []);
    addDieFace(1, [center]);
    addDieFace(2, [topLeft, bottomRight]);
    addDieFace(3, [topLeft, center, bottomRight]);
    addDieFace(4, [topLeft, bottomLeft, topRight, bottomRight]);
    addDieFace(5, [topLeft, bottomLeft, center, topRight, bottomRight]);
    addDieFace(6, [topLeft, midLeft, bottomLeft, topRight, midRight, bottomRight]);
    addDieFace(7, [topLeft, midLeft, bottomLeft, center, topRight, midRight, bottomRight]);
    addDieFace(8, [topLeft, midLeft, bottomLeft, topMid, bottomMid, topRight, midRight, bottomRight]);
    addDieFace(9, [topLeft, midLeft, bottomLeft, topMid, center, bottomMid, topRight, midRight, bottomRight]);
}
function calculateRolls(diceAmount, faceAmount) {
    let rolls = [];
    for (let i = 0; i < diceAmount; i++) {
        const roll = Math.floor(Math.random() * faceAmount) + 1;
        rolls.push(roll);
    }
    return rolls;
}
function calculateSuccess(total, addModifier, difficultyThreshold, diceAmount, faceAmount, criticalMargin) {
    return (total >= difficultyThreshold
        || total > addModifier + diceAmount * faceAmount - criticalMargin)
        && total >= addModifier + diceAmount + criticalMargin;
}
function rollDice() {
    var _a;
    return __awaiter(this, void 0, void 0, function* () {
        const addModifier = +getInputValueById(HtmlIds.addModifier);
        const difficultyThreshold = +getInputValueById(HtmlIds.difficultyThreshold);
        const diceAmount = +getInputValueById(HtmlIds.diceAmount);
        const faceAmount = +getInputValueById(HtmlIds.faceAmount);
        const criticalMargin = +getInputValueById(HtmlIds.criticalMargin);
        const rollButton = getElementById(HtmlIds.rollButton);
        rollButton.disabled = true;
        const rolls = calculateRolls(diceAmount, faceAmount);
        const total = addModifier + rolls.reduce((a, b) => a + b);
        const success = calculateSuccess(total, addModifier, difficultyThreshold, diceAmount, faceAmount, criticalMargin);
        console.log('rolled total:', total, success);
        const resultDice = getElementById(HtmlIds.resultDice);
        const resultText = getElementById(HtmlIds.resultText);
        if (isReducedMotion()) {
            clearChildren(resultDice);
            animate(resultDice, CssAnimation.none);
            animate(resultText, CssAnimation.none);
            setResult(resultText, resultDice, success, rolls, faceAmount);
            rollButton.disabled = false;
            return;
        }
        yield animate(resultText, CssAnimation.furlout);
        yield animate(resultDice, CssAnimation.fadeout);
        clearChildren(resultDice);
        resultDice.style.animationName = '';
        resultDice.hidden = true;
        resultText.hidden = true;
        setResult(resultText, resultDice, success, rolls, faceAmount);
        const flashover = getElementById(success ? HtmlIds.flashoverSuccess : HtmlIds.flashoverFailure);
        const sidebar = getElementById(HtmlIds.sidebar);
        flashover.classList.remove('flashfade');
        sidebar.classList.add('background-up');
        yield playUntilEnd(AudioFiles.spinup);
        (success ? AudioFiles.success : AudioFiles.failure).play();
        flashover.classList.add('flashfade');
        doAfterDelay(115, () => sidebar.classList.remove('background-up'));
        let promise = animate(resultDice, CssAnimation.none);
        promise = (_a = animateDice()) !== null && _a !== void 0 ? _a : promise;
        resultDice.hidden = false;
        yield promise;
        promise = animate(resultText, CssAnimation.furlin);
        resultText.hidden = false;
        yield promise;
        rollButton.disabled = false;
    });
}
function playUntilEnd(media) {
    return __awaiter(this, void 0, void 0, function* () {
        const promise = new Promise(res => media.addEventListener('ended', res, { once: true, passive: true }));
        media.play();
        yield promise;
    });
}
function animateDice() {
    let promise;
    [...document.querySelectorAll('.die')]
        .reverse()
        .forEach((die, i) => {
        die.style.animationDelay = `${i * .2 + .2}s`;
        promise = animate(die, CssAnimation.rollin);
    });
    return promise;
}
function setResult(resultText, resultDice, success, rolls, faceAmount) {
    resultText.textContent = success ? 'Check Success' : 'Check Failure';
    resultText.classList.remove('success');
    resultText.classList.remove('failure');
    resultText.classList.add(success ? 'success' : 'failure');
    for (let i = 0; i < rolls.length; i++) {
        const roll = rolls[i];
        let die;
        if (faceAmount <= 9) {
            die = diceSvgs[roll].cloneNode(true);
        }
        else if (faceAmount === 12 || faceAmount === 20 || faceAmount === 48) {
            let dieCenter = document.createElement('span');
            dieCenter.innerText = roll.toString();
            die = document.createElement('span');
            die.appendChild(dieCenter);
            die.classList.add('die');
            die.classList.add('largeDie');
            die.style.backgroundImage = `url('d${faceAmount}.png')`;
        }
        else {
            die = document.createElement('span');
            die.innerText = roll.toString();
            die.classList.add('die');
        }
        resultDice === null || resultDice === void 0 ? void 0 : resultDice.appendChild(die);
    }
}
(_a = getElementById(HtmlIds.rollButton)) === null || _a === void 0 ? void 0 : _a.addEventListener('click', () => rollDice(), { passive: true });
function setDifficultyPreset() {
    const difficultyPreset = getSelectedValueById(HtmlIds.difficultyPreset);
    const difficultyThresholdInput = getElementById(HtmlIds.difficultyThreshold);
    if (difficultyPreset && difficultyThresholdInput) {
        difficultyThresholdInput.value = difficultyPreset;
    }
}
(_b = getElementById(HtmlIds.difficultyPreset)) === null || _b === void 0 ? void 0 : _b.addEventListener('change', () => setDifficultyPreset(), { passive: true });
function resetDifficultyPreset() {
    const difficultyThreshold = getInputValueById(HtmlIds.difficultyThreshold);
    const selectElement = getElementById(HtmlIds.difficultyPreset);
    for (let i = 0; i < selectElement.childElementCount; i++) {
        const optionElement = selectElement.children[i];
        if (optionElement.value === difficultyThreshold) {
            selectElement.selectedIndex = i;
            return;
        }
    }
    selectElement.selectedIndex = 0;
}
(_c = getElementById(HtmlIds.difficultyThreshold)) === null || _c === void 0 ? void 0 : _c.addEventListener('change', () => resetDifficultyPreset(), { passive: true });
function resetMinDifficulty() {
    const diceAmount = getInputValueById(HtmlIds.diceAmount);
    const difficultyThresholdInput = getElementById(HtmlIds.difficultyThreshold);
    difficultyThresholdInput.min = diceAmount;
    difficultyThresholdInput.value = Math.max(+difficultyThresholdInput.value, +difficultyThresholdInput.min).toString();
    resetDifficultyPreset();
}
(_d = getElementById(HtmlIds.diceAmount)) === null || _d === void 0 ? void 0 : _d.addEventListener('change', () => resetMinDifficulty(), { passive: true });
function resetMaxMargin() {
    const diceAmount = +getInputValueById(HtmlIds.diceAmount);
    const faceAmount = +getInputValueById(HtmlIds.faceAmount);
    const criticalMarginInput = getElementById(HtmlIds.criticalMargin);
    criticalMarginInput.max = Math.ceil((diceAmount * faceAmount - diceAmount) * 0.5).toString();
    criticalMarginInput.value = Math.min(+criticalMarginInput.value, +criticalMarginInput.max).toString();
}
(_e = getElementById(HtmlIds.faceAmount)) === null || _e === void 0 ? void 0 : _e.addEventListener('change', () => resetMaxMargin(), { passive: true });
(_f = getElementById(HtmlIds.diceAmount)) === null || _f === void 0 ? void 0 : _f.addEventListener('change', () => resetMaxMargin(), { passive: true });
if (window.matchMedia('(prefers-reduced-motion)').matches) {
    const reducedMotion = getElementById(HtmlIds.reducedMotion);
    reducedMotion.checked = true;
}
