enum HtmlIds {
    addModifier = 'addModifier',
    difficultyPreset = 'difficultyPreset',
    difficultyThreshold = 'difficultyThreshold',
    reducedMotion = 'reducedMotion',
    diceAmount = 'diceAmount',
    faceAmount = 'faceAmount',
    criticalMargin = 'criticalMargin',
    rollButton = 'rollButton',
    resultText = 'resultText',
    resultDice = 'resultDice',
    flashoverSuccess = 'flashover-success',
    flashoverFailure = 'flashover-failure',
    sidebar = "sidebar"
}

enum CssAnimation {
    none = 'noanim',
    fadeout = 'fadeout',
    furlin = 'furlin',
    furlout = 'furlout',
    rollin = 'rollin',
}

const AudioFiles = {
    spinup: new Audio('./spinup.mp3'),
    success: new Audio('./success.mp3'),
    failure: new Audio('./failure.mp3')
}

function getElementById(id: HtmlIds) {
    return document.querySelector(`#${id}`);
}

function getInputValueById(id: HtmlIds) {
    return (getElementById(id) as HTMLInputElement)?.value;
}

function getSelectedValueById(id: HtmlIds) {
    return (getElementById(id) as HTMLSelectElement)?.value;
}

function isReducedMotion() {
    return (getElementById(HtmlIds.reducedMotion) as HTMLInputElement)?.checked;
}

function animate(node: HTMLElement, animationName: CssAnimation) {
    return new Promise((resolve, reject) => {
        for (const a in CssAnimation) {
            const className = CssAnimation[a as keyof typeof CssAnimation];
            node.classList.remove(className);
        }
        if (animationName == CssAnimation.none) { return; }
        node.classList.add(animationName);
        node.addEventListener('animationend', () => {
            resolve('animationend');
        }, { once: true });
    });
}

function clearChildren(element: Element) {
    while (element.firstChild) {
        element.removeChild(element.lastChild!);
    }
}

function delay(ms: number) {
    return new Promise(res => setTimeout(res, ms));
}

async function doAfterDelay<Out>(ms: number, func: (args: any[]) => Out, ...args: any[]): Promise<Out> {
    await delay(ms);
    return func(args);
}


//TODO move this hunk into a separate file
let diceSvgs: SVGSVGElement[] = [];
{
    const svgns = 'http://www.w3.org/2000/svg';
    const svgcolor = '#fff';

    function generateSvgEye(cx: number, cy: number): SVGCircleElement {
        const eye = document.createElementNS(svgns, 'circle') as SVGCircleElement;
        eye.setAttribute('fill', svgcolor);
        eye.setAttribute('r', '18');
        eye.setAttribute('cx', cx.toString());
        eye.setAttribute('cy', cy.toString());
        return eye;
    }

    const topLeft = generateSvgEye(52, 52);
    const midLeft = generateSvgEye(52, 105);
    const bottomLeft = generateSvgEye(52, 158);
    const topMid = generateSvgEye(105, 52);
    const center = generateSvgEye(105, 105);
    const bottomMid = generateSvgEye(105, 158);
    const topRight = generateSvgEye(158, 52);
    const midRight = generateSvgEye(158, 105);
    const bottomRight = generateSvgEye(158, 158);

    function addDieFace(i: number, eyes: SVGCircleElement[]) {
        const svg = document.createElementNS(svgns, 'svg');
        svg.setAttribute('width', '210');
        svg.setAttribute('height', '210');
        svg.setAttribute('xmlns', svgns);
        svg.setAttribute('class', 'die');
        const title = document.createElementNS(svgns, 'title');
        title.textContent = i.toString();
        svg.appendChild(title);
        const rect = document.createElementNS(svgns, 'rect');
        rect.setAttribute('width', '200');
        rect.setAttribute('height', '200');
        rect.setAttribute('fill', 'none')
        rect.setAttribute('stroke', svgcolor);
        rect.setAttribute('stroke-width', '10');
        rect.setAttribute('x', '5');
        rect.setAttribute('y', '5');
        rect.setAttribute('rx', '30');
        svg.appendChild(rect);
        eyes.forEach(e => {
            svg.appendChild(e.cloneNode());
        });
        diceSvgs[i] = svg;
    }

    addDieFace(0, []);
    addDieFace(1, [center]);
    addDieFace(2, [topLeft, bottomRight]);
    addDieFace(3, [topLeft, center, bottomRight]);
    addDieFace(4, [topLeft, bottomLeft, topRight, bottomRight]);
    addDieFace(5, [topLeft, bottomLeft, center, topRight, bottomRight]);
    addDieFace(6, [topLeft, midLeft, bottomLeft, topRight, midRight, bottomRight]);
    addDieFace(7, [topLeft, midLeft, bottomLeft, center, topRight, midRight, bottomRight]);
    addDieFace(8, [topLeft, midLeft, bottomLeft, topMid, bottomMid, topRight, midRight, bottomRight]);
    addDieFace(9, [topLeft, midLeft, bottomLeft, topMid, center, bottomMid, topRight, midRight, bottomRight]);
}


function calculateRolls(diceAmount: number, faceAmount: number) {
    let rolls: number[] = [];
    for (let i = 0; i < diceAmount; i++) {
        const roll = Math.floor(Math.random() * faceAmount) + 1;
        rolls.push(roll);
    }
    return rolls;
}

function calculateSuccess(
    total: number,
    addModifier: number,
    difficultyThreshold: number,
    diceAmount: number,
    faceAmount: number,
    criticalMargin: number
) {
    return (total >= difficultyThreshold
        || total > addModifier + diceAmount * faceAmount - criticalMargin)
        && total >= addModifier + diceAmount + criticalMargin;
}

async function rollDice() {
    const addModifier = +getInputValueById(HtmlIds.addModifier);
    const difficultyThreshold = +getInputValueById(HtmlIds.difficultyThreshold);
    const diceAmount = +getInputValueById(HtmlIds.diceAmount);
    const faceAmount = +getInputValueById(HtmlIds.faceAmount);
    const criticalMargin = +getInputValueById(HtmlIds.criticalMargin);
    const rollButton = getElementById(HtmlIds.rollButton) as HTMLButtonElement;

    rollButton.disabled = true;

    const rolls = calculateRolls(diceAmount, faceAmount);
    const total = addModifier + rolls.reduce((a, b) => a + b);
    const success = calculateSuccess(total, addModifier, difficultyThreshold, diceAmount, faceAmount, criticalMargin);

    console.log('rolled total:', total, success);

    const resultDice = getElementById(HtmlIds.resultDice) as HTMLElement;
    const resultText = getElementById(HtmlIds.resultText) as HTMLElement;

    if (isReducedMotion()) {
        clearChildren(resultDice);
        animate(resultDice, CssAnimation.none);
        animate(resultText, CssAnimation.none);
        setResult(resultText, resultDice, success, rolls, faceAmount);
        rollButton.disabled = false;
        return;
    }


    await animate(resultText, CssAnimation.furlout);
    await animate(resultDice, CssAnimation.fadeout);

    clearChildren(resultDice);
    resultDice.style.animationName = '';
    resultDice.hidden = true;
    resultText.hidden = true;
    setResult(resultText, resultDice, success, rolls, faceAmount);

    const flashover = getElementById(success ? HtmlIds.flashoverSuccess : HtmlIds.flashoverFailure) as HTMLElement;
    const sidebar = getElementById(HtmlIds.sidebar) as HTMLElement;

    flashover.classList.remove('flashfade');
    sidebar.classList.add('background-up');

    await playUntilEnd(AudioFiles.spinup);
    (success ? AudioFiles.success : AudioFiles.failure).play();

    flashover.classList.add('flashfade');
    doAfterDelay(115, () => sidebar.classList.remove('background-up'));

    let promise = animate(resultDice, CssAnimation.none);
    promise = animateDice() ?? promise;
    resultDice.hidden = false;
    await promise;

    promise = animate(resultText, CssAnimation.furlin);
    resultText.hidden = false;
    await promise;

    rollButton.disabled = false;
}

async function playUntilEnd(media: HTMLMediaElement) {
    const promise = new Promise(res => media.addEventListener('ended', res, {once: true, passive: true}));
    media.play();
    await promise;
}

function animateDice() {
    let promise: Promise<unknown> | undefined;
    ([...document.querySelectorAll('.die')] as HTMLElement[])
        .reverse()
        .forEach((die, i) => {
            die.style.animationDelay = `${i * .2 + .2}s`;
            promise = animate(die as HTMLElement, CssAnimation.rollin);
        });
    return promise;
}

function setResult(resultText: HTMLElement, resultDice: HTMLElement, success: boolean, rolls: number[], faceAmount: number) {
    resultText.textContent = success ? 'Check Success' : 'Check Failure';
    resultText.classList.remove('success');
    resultText.classList.remove('failure');
    resultText.classList.add(success ? 'success' : 'failure');

    for (let i = 0; i < rolls.length; i++) {
        const roll = rolls[i];
        let die: HTMLElement;
        if (faceAmount <= 9) {
            die = diceSvgs[roll].cloneNode(true) as HTMLElement;
        }
        else if (faceAmount === 12 || faceAmount === 20 || faceAmount === 48) {
            let dieCenter = document.createElement('span');
            dieCenter.innerText = roll.toString();
            die = document.createElement('span');
            die.appendChild(dieCenter);
            die.classList.add('die');
            die.classList.add('largeDie');
            die.style.backgroundImage = `url('d${faceAmount}.png')`
        }
        else {
            die = document.createElement('span');
            die.innerText = roll.toString();
            die.classList.add('die');
        }
        resultDice?.appendChild(die);
    }
}

getElementById(HtmlIds.rollButton)?.addEventListener('click', () => rollDice(), {passive: true});


function setDifficultyPreset() {
    const difficultyPreset = getSelectedValueById(HtmlIds.difficultyPreset);
    const difficultyThresholdInput = getElementById(HtmlIds.difficultyThreshold) as HTMLInputElement;
    if (difficultyPreset && difficultyThresholdInput) {
        difficultyThresholdInput.value = difficultyPreset;
    }
}

getElementById(HtmlIds.difficultyPreset)?.addEventListener('change', () => setDifficultyPreset(), {passive: true});


function resetDifficultyPreset() {
    const difficultyThreshold = getInputValueById(HtmlIds.difficultyThreshold);
    const selectElement = getElementById(HtmlIds.difficultyPreset) as HTMLSelectElement;
    for (let i = 0; i < selectElement.childElementCount; i++) {
        const optionElement = selectElement.children[i] as HTMLOptionElement;
        if (optionElement.value === difficultyThreshold) {
            selectElement.selectedIndex = i;
            return;
        }
    }
    selectElement.selectedIndex = 0;
}

getElementById(HtmlIds.difficultyThreshold)?.addEventListener('change', () => resetDifficultyPreset(), {passive: true});


function resetMinDifficulty() {
    const diceAmount = getInputValueById(HtmlIds.diceAmount);
    const difficultyThresholdInput = getElementById(HtmlIds.difficultyThreshold) as HTMLInputElement;
    difficultyThresholdInput.min = diceAmount;
    difficultyThresholdInput.value = Math.max(+difficultyThresholdInput.value, +difficultyThresholdInput.min).toString();
    resetDifficultyPreset();
}

getElementById(HtmlIds.diceAmount)?.addEventListener('change', () => resetMinDifficulty(), {passive: true});


function resetMaxMargin() {
    const diceAmount = +getInputValueById(HtmlIds.diceAmount);
    const faceAmount = +getInputValueById(HtmlIds.faceAmount);
    const criticalMarginInput = getElementById(HtmlIds.criticalMargin) as HTMLInputElement;
    criticalMarginInput.max = Math.ceil((diceAmount * faceAmount - diceAmount) * 0.5).toString();
    criticalMarginInput.value = Math.min(+criticalMarginInput.value, +criticalMarginInput.max).toString();
}

getElementById(HtmlIds.faceAmount)?.addEventListener('change', () => resetMaxMargin(), {passive: true});
getElementById(HtmlIds.diceAmount)?.addEventListener('change', () => resetMaxMargin(), {passive: true});

if (window.matchMedia('(prefers-reduced-motion)').matches) {
    const reducedMotion = getElementById(HtmlIds.reducedMotion) as HTMLInputElement;
    reducedMotion.checked = true;
}
