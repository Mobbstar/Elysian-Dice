# Elysian Dice

[Try it out!](https://mobbstar.gitlab.io/Elysian-Dice)

This is a simple client-side web-app meant for [TTRPGs](https://en.wikipedia.org/wiki/Tabletop_role-playing_game). It allows rolling a handful of dice against an arbitrary target total. The default settings are tuned to [Disco Elysium](https://discoelysium.com/).

## Contribution

[Feature requests, issue reports](https://gitlab.com/Mobbstar/Elysian-Dice/-/issues), and forks are welcome.
